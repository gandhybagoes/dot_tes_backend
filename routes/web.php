<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Session::get('login')){
        return Redirect::to('/lowongan');
    }
    return view('auth.login');
});



Route::get('/login', function () {
	if(Session::get('login')){
        return Redirect::to('/lowongan');
    }
    return view('auth.login');
});
Route::get('/register', function () {
	if(Session::get('login')){
        return Redirect::to('/lowongan');
    }
    return view('auth.register');
});
Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

Route::get('/lowongan', 'LowonganController@index');
Route::post('/lowongan/create', 'LowonganController@create');
Route::get('/lowongan/{id}/edit', 'LowonganController@edit');
Route::post('/lowongan/{id}/update', 'LowonganController@update');
Route::get('/lowongan/{id}/delete', 'LowonganController@delete');

Route::get('/dashboard', 'DashboardController@index');
Route::get('/apply/{id}', 'DashboardController@apply');
Route::post('/apply/proses', 'DashboardController@proses_apply');

Route::get('/kandidat', 'KandidatController@index');
Route::get('/kandidat/download/{file}', 'KandidatController@download_berkas');
Route::get('/kandidat/{id}/terima', 'KandidatController@terima');
Route::get('/kandidat/{id}/tolak', 'KandidatController@tolak');