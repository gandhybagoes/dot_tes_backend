@extends('layout.master_login')

<div class="register-box">
  @section('content')
  <body class="hold-transition register-page">
  @if(session('sukses'))
  <div class="alert alert-success" role="alert">
    {{session('sukses')}}
  </div>
  @elseif(session('gagal'))
  <div class="alert alert-danger" role="alert">
    {{session('gagal')}}
  </div>
  @endif
  <div class="register-logo">
    <a href="/"><b>HRD </b>APPS</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new akun</p>

    <form action="/register" method="post">
      {{ csrf_field() }}
      <div class="form-group has-feedback">
        <label>Nama</label>
        <input type="text" name="nama" class="form-control" placeholder="Nama" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>NIK</label>
        <input type="text" name="nik" class="form-control" placeholder="NIK" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>PASSWORD</label>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>REPASSWORD</label>
        <input type="password" name="repassword" class="form-control" placeholder="Retype Password" required>
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>  
    </div>

    <a href="/" class="text-center">I already have a akun</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.login-box -->
@endsection

@section('script_content')
  <script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 4000);
    
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  </script>
@endsection