@extends('layout.master_dashboard')

@section('content')
<style type="text/css">
  pre {border: 0; background-color: transparent;}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $judul }}
  </h1>
</section>

<!-- Main content -->
<section class="content">
  @if(session('sukses'))
  <div class="alert alert-success" role="alert">
    {{session('sukses')}}
  </div>
  @elseif(session('gagal'))
  <div class="alert alert-danger" role="alert">
    {{session('gagal')}}
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <a href="/dashboard"><button type="button" class="btn btn-primary float-right btn-sm">Kembali Ke Daftar Lowongan</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form action="/apply/proses" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama_kandidat" class="form-control" id="exampleInputEmail1" required>
              </div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                <select class="form-control" name="jenis_kelamin" id="exampleFormControlSelect1">
                  <option value="laki-laki">Laki-laki</option>
                  <option value="perempuan">Perempuan</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Alamat</label>
                <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" rows="3" required></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nomor Telepon</label>
                <input type="text" name="nomor_telepon" class="form-control" id="exampleInputEmail1" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Lowongan yang diapply</label>
                <input type="text" name="lowongan_name" value="{{ $lowongan->nama_lowongan }}" class="form-control" id="exampleInputEmail1" readonly>
                <input type="hidden" name="lowongan_id" value="{{ $lowongan->id }}">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Upload CV</label>
                <input type="file" name="berkas" class="form-control" id="exampleInputEmail1" required>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Apply</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('script_content')
<script>
  window.setTimeout(function() {
      $(".alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove(); 
      });
  }, 4000);
</script>
@endsection