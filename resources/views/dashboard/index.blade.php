@extends('layout.master_dashboard')

@section('content')
<style type="text/css">
  pre {border: 0; background-color: transparent;}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $judul }}
  </h1>
</section>

<!-- Main content -->
<section class="content">
  @if(session('sukses'))
  <div class="alert alert-success" role="alert">
    {{session('sukses')}}
  </div>
  @elseif(session('gagal'))
  <div class="alert alert-danger" role="alert">
    {{session('gagal')}}
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Nama Lowongan</th>
              <th>Job Description</th>
              <th>Skill Requirement</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data_lowongan as $lowongan)
            <tr>
              <td>{{ $lowongan->nama_lowongan }}</td>
              <td><pre>{{ $lowongan->jobdesc }}</pre></td>
              <td><pre>{{ $lowongan->skill }}</pre></td>
              <td>
                <center>
                  <a href="/apply/{{ $lowongan->id }}" class="btn btn-warning btn-sm">Apply</a>
                </center>
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('script_content')
<script>
  window.setTimeout(function() {
      $(".alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove(); 
      });
  }, 4000);
  $(function () {
    $('#example1').DataTable()
  })
</script>
@endsection