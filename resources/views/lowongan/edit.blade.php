@extends('layout.master_template')

@section('content')
<style type="text/css">
	pre {border: 0; background-color: transparent;}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $judul }}
  </h1>
</section>

<!-- Main content -->
<section class="content">
  @if(session('sukses'))
  <div class="alert alert-success" role="alert">
    {{session('sukses')}}
  </div>
  @elseif(session('gagal'))
  <div class="alert alert-danger" role="alert">
    {{session('gagal')}}
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <a href="/lowongan"><button type="button" class="btn btn-primary float-right btn-sm">Kembali Ke Daftar Lowongan</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form action="/lowongan/{{ $lowongan->id }}/update" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
              <input type="hidden" name="is_delete" value="{{ $lowongan->is_delete }}">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Lowongan</label>
                <input type="text" name="nama_lowongan" value="{{ $lowongan->nama_lowongan }}" class="form-control" id="exampleInputEmail1" required>
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Job Description</label>
                <textarea class="form-control" name="jobdesc" id="exampleFormControlTextarea1" rows="3">{{ $lowongan->jobdesc }}</textarea>
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Skill Requirement</label>
                <textarea class="form-control" name="skill" id="exampleFormControlTextarea1" rows="3">{{ $lowongan->skill }}</textarea>
              </div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Status</label>
                <select class="form-control" name="status" id="exampleFormControlSelect1">
                  <option value="publish" @if($lowongan->status == 'publish') selected @endif>Publish</option>
                  <option value="unpublished" @if($lowongan->status == 'unpublished') selected @endif>Unpublished</option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('script_content')
<script>
	window.setTimeout(function() {
	    $(".alert").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 4000);
</script>
@endsection