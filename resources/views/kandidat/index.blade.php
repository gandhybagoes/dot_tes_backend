@extends('layout.master_template')

@section('content')
<style type="text/css">
	pre {border: 0; background-color: transparent;}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $judul }}
  </h1>
</section>

<!-- Main content -->
<section class="content">
  @if(session('sukses'))
  <div class="alert alert-success" role="alert">
    {{session('sukses')}}
  </div>
  @elseif(session('gagal'))
  <div class="alert alert-danger" role="alert">
    {{session('gagal')}}
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Nama Kandidat</th>
              <th>Jenis Kelamin</th>
              <th>Alamat</th>
              <th>Nomor Telepon</th>
              <th>Email</th>
              <th>Posisi Apply</th>
              <th>Tanggal Apply</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data_kandidat as $kandidat)
            <tr>
              <td>{{ $kandidat->nama_kandidat }}</td>
              <td>{{ $kandidat->jenis_kelamin }}</td>
              <td><pre>{{ $kandidat->alamat }}</pre></td>
              <td>{{ $kandidat->nomor_telepon }}</td>
              <td>{{ $kandidat->email }}</td>
              <td> 
                @if($kandidat->is_delete) 
                  {{ $kandidat->nama_lowongan }} <br>(Lowongan Sudah dihapus)
                @else 
                  {{ $kandidat->nama_lowongan }}
                @endif 
              </td>
              <td>{{ $kandidat->created_at }}</td>
              <td>
                @if($kandidat->status == 'review')
                  <label class="label bg-blue">{{ $kandidat->status }}</label>
                @elseif($kandidat->status == 'diterima')
                  <label class="label bg-green">{{ $kandidat->status }}</label>
                @else
                  <label class="label bg-red">{{ $kandidat->status }}</label>
                @endif
              </td>
              <td>
              	<center>
                @if($kandidat->status == 'review')
              	<a href="/kandidat/{{ $kandidat->id }}/terima" class="btn btn-success btn-sm" onclick="return confirm('Apakah Anda akan menerima kandidat ini ?')">Terima</a>
                <a href="/kandidat/{{ $kandidat->id }}/tolak" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda akan menolak kandidat ini ?')">Tolak</a>
                <br><br>
                <a href="/kandidat/download/{{ $kandidat->file_name }}" target="_blank" class="btn btn-default btn-sm">Download Berkas</a>
                @else
                <a href="/kandidat/download/{{ $kandidat->file_name }}" target="_blank" class="btn btn-default btn-sm">Download Berkas</a>
                @endif
                </center>
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <form action="/lowongan/create" method="post">
     	{{ csrf_field() }}
      <div class="modal-body">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Nama Lowongan</label>
		    <input type="text" name="nama_lowongan" class="form-control" id="exampleInputEmail1" required>
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlTextarea1">Job Description</label>
		    <textarea class="form-control" name="jobdesc" id="exampleFormControlTextarea1" rows="3"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlTextarea1">Skill Requirement</label>
		    <textarea class="form-control" name="skill" id="exampleFormControlTextarea1" rows="3"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlSelect1">Status</label>
		    <select class="form-control" name="status" id="exampleFormControlSelect1">
		      <option value="publish">Publish</option>
		      <option value="unpublished">Unpublished</option>
		    </select>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  	</form>
    </div>
  </div>
</div>
@endsection

@section('script_content')
<script>
	window.setTimeout(function() {
	    $(".alert").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 4000);
	$(function () {
		$('#example1').DataTable()
	})
</script>
@endsection