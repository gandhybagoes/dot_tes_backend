<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kandidat extends Model
{
    protected $table = 'kandidat';
    protected $fillable = ['nama_kandidat', 'jenis_kelamin', 'alamat', 'nomor_telepon', 'email', 'lowongan_id', 'status', 'file_name'];
}
