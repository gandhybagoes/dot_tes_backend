<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    protected $table = 'lowongan';
    protected $fillable = ['nama_lowongan','jobdesc','skill','status','is_delete'];
}
