<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Redirector;
use App\Lowongan;
use App\Kandidat;

class KandidatController extends Controller
{
	public function __construct(Redirector $redirect, Session $session)
    {
    	$this->middleware(function ($request, $next) {
	        if(!Session::get('login')){
	            return redirect('/login');
	        }else{
	            return $next($request);
	        }
	    });
    }

    public function index()
    {
    	$data_kandidat = Kandidat::leftJoin('lowongan', 'lowongan.id', '=', 'kandidat.lowongan_id')
    		->select('kandidat.*','lowongan.nama_lowongan','lowongan.is_delete')
            ->get();
    	return view('kandidat.index',[
    		'data_kandidat' => $data_kandidat,
    		'judul' => 'Daftar Kandidat'
    	]);
    }

    public function download_berkas($filename)
    {
    	$file= public_path(). "\upload\\$filename";

    	$headers = array(
              'Content-Type: application/pdf',
        );

    	return Response::download($file, $filename, $headers);
    }

    public function terima($id)
    {
    	$kandidat = Kandidat::find($id);
    	$kandidat->status = 'diterima';
    	$kandidat->save();
    	return redirect('/kandidat')->with('sukses', 'Kandidat diterima');
    }

    public function tolak($id)
    {
    	$kandidat = Kandidat::find($id);
    	$kandidat->status = 'ditolak';
    	$kandidat->save();
    	return redirect('/kandidat')->with('sukses', 'Kandidat ditolak');
    }
}
