<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lowongan;
use App\Kandidat;

date_default_timezone_set("Asia/Jakarta");
class DashboardController extends Controller
{
    public function index()
    {
    	$data_lowongan = Lowongan::where('is_delete',0)->get();
    	return view('dashboard.index',[
    		'data_lowongan' => $data_lowongan,
    		'judul' => 'Daftar Lowongan Pekerjaan'
    	]);
    }

    public function apply($id)
    {
    	$lowongan = Lowongan::find($id);
    	return view('dashboard.apply',[
    		'lowongan' => $lowongan,
    		'judul' => 'Form Lamaran Pekerjaan'
    	]);
    }

    public function proses_apply(Request $request)
    {
    	$file = $request->file('berkas');
    	$nama_file = explode(".", $file->getClientOriginalName());
    	$nama_file = "berkas_".$request->nama_kandidat."_".date('Y-m-d_H-i-s').".".$nama_file[1];
    	$nama_file = $file->getClientOriginalName();
  
		//Move Uploaded File
		$destinationPath = 'upload';
		$file->move($destinationPath,$nama_file);

		$data =  new Kandidat();
        $data->nama_kandidat = $request->nama_kandidat;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->alamat = $request->alamat;
        $data->nomor_telepon = $request->nomor_telepon;
        $data->email = $request->email;
        $data->lowongan_id = $request->lowongan_id;
        $data->status = 'review';
        $data->file_name = $nama_file;
        $data->save();
    	return redirect('/dashboard')->with('sukses', 'Lamaran pekerjaan berhasil di apply');
    }
}
