<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\M_User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
	    if(Session::get('login')){
	        return Redirect::to('/lowongan');
	    }
    	$nama = $request->nama;
    	$nik = $request->nik;
    	$password = $request->password;
    	$repassword = $request->repassword;

    	if($password != $repassword){
    		return redirect('/register')->with('gagal', 'Password harus sama dengan Repassword');
    	}

    	$cek_user = M_User::where('nik',$nik)->first();
    	if($cek_user){
    		return redirect('/register')->with('gagal', 'NIK yang Anda masukkan sudah digunakan orang lain');
    	}

    	$data =  new M_User();
        $data->nama = $nama;
        $data->nik = $nik;
        $data->password = bcrypt($password);
        $data->save();
    	return redirect('/login')->with('sukses', 'Akun berhasil dibuat, silahkan untuk login');	
    }

    public function login(Request $request)
    {
    	if(Session::get('login')){
	        return Redirect::to('/lowongan');
	    }
    	$nik = $request->nik;
    	$password = $request->password;

    	$data = M_User::where('nik',$nik)->first();
        if($data){
            if(Hash::check($password,$data->password)){
                Session::put('nama',$data->nama);
                Session::put('nik',$data->nik);
                Session::put('login',TRUE);
                return redirect('/lowongan');
            }
            else{
                return redirect('/login')->with('gagal','Password Salah!');
            }
        }
        else{
            return redirect('login')->with('gagal','Data NIK tidak ditemukan!');
        }
    }

    public function logout()
    {
    	Session::flush();
        return redirect('/login');
    }
}
