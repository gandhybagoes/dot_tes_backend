<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing\Redirector;
use App\Lowongan;

class LowonganController extends Controller
{
	public function __construct(Redirector $redirect, Session $session)
    {
    	$this->middleware(function ($request, $next) {
	        if(!Session::get('login')){
	            return redirect('/login');
	        }else{
	            return $next($request);
	        }
	    });
    }

    public function index()
    {
    	$data_lowongan = Lowongan::where('is_delete',0)->get();
    	return view('lowongan.index',[
    		'data_lowongan' => $data_lowongan,
    		'judul' => 'Daftar Lowongan Pekerjaan'
    	]);
    }

    public function create(Request $request)
    {
    	$nama_lowongan = $request->nama_lowongan;
    	$jobdesc = $request->jobdesc;
    	$skill = $request->skill;
    	$status = $request->status;
    	$is_delete = 0;

    	$data =  new Lowongan();
        $data->nama_lowongan = $request->nama_lowongan;
        $data->jobdesc = $request->jobdesc;
        $data->skill = $request->skill;
        $data->status = $request->status;
        $data->is_delete = 0;
        $data->save();
    	return redirect('/lowongan')->with('sukses', 'Data lowongan pekerjaan berhasil dibuat');
    }

    public function edit($id)
    {
    	$lowongan = Lowongan::find($id);
    	return view('lowongan.edit',[
    		'lowongan' => $lowongan,
    		'judul' => 'Edit Lowongan Pekerjaan'
    	]);
    }

    public function update(Request $request, $id)
    {
    	$lowongan = Lowongan::find($id);
    	$lowongan->update($request->all());
    	return redirect('/lowongan')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
    	$lowongan = Lowongan::find($id);
    	$lowongan->is_delete = 1;
    	$lowongan->save();
    	return redirect('/lowongan')->with('sukses', 'Data berhasil dihapus');
    }
}
