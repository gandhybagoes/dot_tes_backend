## Knowledge & Experience Questions

1. Apa yang anda ketahui dengan struktur MVC? Jelaskan dan mengapa struktur tersebut umum dipakai dan disarankan di web development
- MVC sepemahaman saya adalah sebuah metode untuk pembuatan aplikasi dimana dimana fokus dari metode tersebut adalah untuk memisahkan antara tugas file satu dengan file lain. Contoh simple nya ada beberapa file yang bertugas untuk menampilkan view, mengambil data dari database, dan memproses data.

Metode ini banyak digunakan karena dapat memudahkan programmer untuk memanage dan memaintenance source code program

2. Apa yang anda ketahui dengan gitflow? 
- Git flow sepemahaman saya adalah sebuah alur dimana kita bisa memanagement model percabangan

3. Apakah anda terbiasa menggunakan framework? Mengapa anda memilih framework tersebut?
- Iya saya terbiasa menggunakan framework CodeIgniter, karena framework tersebut menurut saya mudah untuk dipelajari dan mudah untuk di install, dan juga salah satu framework yang pengembangannya cukup update dan banyak di gunakan di Indonesia

4. Bagaimana anda membuat design database yang baik?
- Saya akan mengusahakan untuk membuat ERD terlebih dahulu untuk desain awal database

5. Tools apa saja yang biasa anda gunakan dalam web development?
- xampp
- sublime
- Git Kraken
- postman
- web browser
- cmd / terminal
- navicat
