<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Cara Install

- clone / download repository ini
- lalu tempatkan di folder webserver anda
- atau bisa juga dengan masuk ke terminal folder tersebut, lalu jalankan perintah **php artisan servre**
- buat database dengan nama 'hrd_apps'
- sesuaikan setting file **.env** dengan konfigurasi database anda
- import database yang ada di folder project ke database yang sudah anda buat tadi
- jika semua sudah, anda bisa membuka aplikasinya di **localhost:8000**

## Cara Penggunaan

- login dengan NIK : 11111 & PASSWORD : admin atau anda bisa buat akun anda sendiri di menu register pada awal tampilan
- untuk CRUD Lowongan ada di menu Lowongan
- untul CRUD Kandidat ada di menu Dashboard Apply (untuk apply lowongan) dan Kandidat (untuk melihat semua data kandidat yang apply lowongan pekerjaan)

## CATATAN

- untuk soal tes logika ada di folder **SOAL_TES/logika.php**
- untuk soal tes **Knowledge & Experience Questions** ada di folder **SOAL_TES/README.md**